# EZVC-README.md

This project installs a scaleable BigBlueButton cluster on AWS.  It uses a custom [bbb-install](https://bitbucket.org/digitalfrontiersmedia-dev/bbb-install/src/master/) script to customize meeting parameters.

## Required:

- A linux command line with AWS and profile installed
- Your AWS profile name 
- BBB Administrator email
- Ezvc subdomain bbb-aws.my-ezvc-domain.com with subdomain DNS delagated to AWS Route53
- The Route53 zone id found here https://console.aws.amazon.com/route53/v2/hostedzones#
- A unique stack name. Use `bbb-aws` unless already used. Check for stack name usage here: https://console.aws.amazon.com/cloudformation/home?region=us-east-1#/stacks?filteringText=&filteringStatus=active&viewNested=true&hideStacks=false

## Running Setup

Syntax:

```
./setup.sh -e admin@my-ezvc-domain.com -p my_profile_name -h 1XXX-MY-ZONE-ID-XXZXXXX -s bbb-aws -d bbb-aws.my-ezvc-domain.com

```

Example:

```
./setup.sh -e admin@wfevent.com -p css -h Z07064-REPLACE-THIS-AFTER-DNS-SETUP-JZOWW -s bbb-aws -d bbb-aws.wfevent.com
```

## Where is the BBB API Secret aka the `Security Salt` ?

The bbb `Security Salt` required on `/admin/config/media/bigbluebutton` can be found in the AWS Secrets manager under `BBBLoadBalancerSecret-xxxxx` here: https://console.aws.amazon.com/secretsmanager/home?region=us-east-1#/listSecrets

## Updating from upstream aws-scalable-big-blue-button-example project

This project customizes the [[https://github.com/aws-samples/aws-scalable-big-blue-button-example | aws-scalable-big-blue-button-example ]] project found on github. Here's how to pull in upstream changes.

```
# Clone this repository
git clone git@bitbucket.org:digitalfrontiersmedia-dev/bbb-aws.git

cd bbb-aws

# Track the ezvc branch
git checkout --track origin/ezvc

# Move back to main branch
git checkout main

# Add the upstream project as remote
git remote add upstream https://github.com/aws-samples/aws-scalable-big-blue-button-example.git

# Pull the upstream changes
git pull upstream main
git add -A
git commit -m "Changes to upstream project v x.x.x"

# Merge the EZVC config changes
TODO: Add git merge magic here.

```